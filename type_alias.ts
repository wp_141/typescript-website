type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    typr: CarType,
    model: CarModel,
}

const carYear: CarYear = 2001;
const carType: CarType = "Toyota";
const carModel: CarModel = "Corolla";

const car1: Car = {
    year: carYear,
    typr: carType,
    model: carModel,
}
console.log(car1)

// const car1: Car = {
//     year: 2001,
//     typr: "Nissan",
//     model: "XXX",
// }

